# Invoicer

## Create your personal invoice
---
<b>Roadmap:</m>

1. [x] Create a user interface

2. [x] Implement the export mechanism

3. [x] Publish it to flathub

4. [ ] Publish it for Windows

---



![image](gallery/overview.png)

![image](gallery/timed.png)

![image](gallery/quoted.png)

![image](gallery/balance.png)

---

## Installation:

* Linux: This app is [available on Flathub](https://flathub.org/apps/details/com.gitlab.adnan338.Invoicer). 

    `https://flathub.org/apps/details/com.gitlab.adnan338.Invoicer`

* Windows: (TODO)

---

## Development:

Please consult the `CONTRIBUTING.md` before jumping in. Feel free to use or patch the `Makefile` to your liking.

---

## Attribution:

<div>Icons made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
