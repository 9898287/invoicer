import locks
import frontend/gui, backend/datastate
import gintro/[gtk, gio, gobject]

when isMainModule:
  let application = newApplication("com.gitlab.adnan338.Invoicer")
  application.connect("activate", appActivate)
  discard application.run()
  withLock dataLock: dumpData(data)