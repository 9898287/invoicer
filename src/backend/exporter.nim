from htmlgen import nil
import tables, times
import datastate, jobunit, helper

proc tableGen(data: Data): string =
    var tableBody: string
    for id in data.shifts.keys():
        tableBody.add htmlgen.tr(
            htmlgen.td(data.shifts[id].getDateString()),
            htmlgen.td(data.shifts[id].description),
            htmlgen.td(data.shifts[id].getStartTimeString()),
            htmlgen.td(data.shifts[id].getFinishTimeString()),
            htmlgen.td(data.shifts[id].getBillableHoursOrQtyString()),
            htmlgen.td(data.shifts[id].getRateString()),
            htmlgen.td(data.shifts[id].getPayableString())
        )
    tableBody.add htmlgen.tr(
        htmlgen.td(
            colspan = "6",
            style = "text-align:left;",
            "Total Payable:"
        ),
        htmlgen.td(
            style = """background-color: #FFEAED;""",
            data.getTotalPayableString()
        )
    )
    tableBody.add htmlgen.tr(
        htmlgen.td(
            colspan = "6",
            style = "text-align:left;",
            "Outstanding Balance:"
        ),
        htmlgen.td(
            style = """background-color: #EAFFED;""",
            data.getBalanceString()
        )
    )
    var tableFooter: string
    tableFooter.add htmlgen.tr(
        htmlgen.td(
            colspan = "6",
            style = "text-align:left;",
            "Net Payable:"
        ),
        htmlgen.td(
            style = "background-color: pink",
            data.getNetPayableString()
        )
    )

    result.add htmlgen.table(
        htmlgen.thead(
            htmlgen.tr(
                htmlgen.th("Date"),
                htmlgen.th("Address/Description"),
                htmlgen.th("Start"),
                htmlgen.th("End"),
                htmlgen.th("Billable Hours/Unit Qty"),
                htmlgen.th("Rate"),
                htmlgen.th("Payable"),
                
        ),
        tableBody,
        tableFooter
    )
    )


proc exportHTML*(data: Data; invoiceTo, employeeName, exportPath: string): string =
    var htmlStr: string
    let styleblock = """
    <style>
        .content {
            max-width: 210mm;
            max-height: 297mm;
            margin: auto;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif, sans;
            position: relative;
        }

        .logo {
            position: relative;
        }

        .images {
            position: absolute;
            top: -10px;
            right: 60px;
            max-height: 25mm;
        }

        h4 {
            text-decoration: underline;
            font-size: 1.2vw;
        }

        h3 {
            font-size: 1.25vw;
        }

        h2 {
            font-size: 1.45vw;
        }

        h1 {
            font-size: 1.8vw;
        }

        th {
            border: 1px solid black;
            text-align: center;
            padding: 5px;
            font-size: 1.2vw;
            font-weight: bold;
        }

        tr,
        td {
            border: 1.1px solid black;
            text-align: center;
            padding: 5px;
            font-weight: normal;
            font-size: 1.2vw;
        }
    </style>
"""
    htmlStr.add htmlgen.head(styleblock)
    htmlStr.add htmlgen.body(
        onload = "window.print()",
        htmlgen.div(
            class = "content",
            htmlgen.div(
                htmlgen.img(
                    src = "../resources/logo.svg",
                    alt = "",
                    class = "image"
                ),
            htmlgen.h1(
                "Invoiced To: " & invoiceTo
            ),
            htmlgen.h2(
                "Employee Time Sheet"
            ),
            htmlgen.h3(
                "Billing Cycle: " & data.getBillingCycleString()
            ),
            htmlgen.h4(
                "Invoiced by: " & employeeName
            ),
            htmlgen.h4(
                "Issue Date: " & getTime().format("dd MMM yyyy")
            ),
            tableGen(data)
        ),
    ),
        
    )
    let filename = exportPath & RuntimeDirSep & "Invoice cycle " &
            data.getBillingCycleString() & " " & $getTime().toUnix()
    filename.writeFile(htmlStr)
    result = filename
