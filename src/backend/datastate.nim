import tables, strutils, options, times, os
import jobunit, helper

type
    Data* = object
        shifts*: OrderedTable[int64, Shift]
        balance*: float
    AppData* = Data

proc getBalanceString*(data: Data): string =
    "$ " & data.balance.formatFloat(ffDecimal, precision = 2)

proc getTotalPayable*(data: Data): float =
    for _, shift in data.shifts:
        result += shift.payable()

proc getTotalPayableString*(data: Data): string =
    "$ " & data.getTotalPayable().formatFloat(ffDecimal, precision = 2)

proc getNetPayable*(data: Data): float =
    data.getTotalPayable - data.balance

proc getNetPayableString*(data: Data): string =
    "$ " & data.getNetPayable().formatFloat(ffDecimal, precision = 2)


proc getBillingCycleString*(data: Data): string =
    var earliest, latest: Option[times.DateTime]
    for _, shift in data.shifts:
        if earliest.isNone() or earliest.get() > shift.date:
            earliest = shift.date.some()
        if latest.isNone() or latest.get() < shift.date:
            latest = shift.date.some()
    earliest.get().format("dd MMM yy") & " to " & latest.get().format("dd MMM yy")

const
    DATE_FORMAT = "yyyy-MM-dd"
    DATE_TIME_FORMAT = "yyyy-MM-dd mm hh tt"

###### MUST USE --gc:arc COMPILER BUG OTHERWISE ######
proc loadData*(data: var Data) =
    if not fileExists(AppDataFilePath):
        echo "Loading data: AppData file not found. Creating..."
        try:
            discard open(AppDataFilePath, fmReadWrite)
            return
        except:
            echo "AppData Creation Unsuccessful: ", getCurrentException().msg
            echo "Running program with no persistent records."
            return
    try:
        var file = AppDataFilePath.open(fmRead)
        data.balance = file.readLine().parseFloat()
        while true:
            try:
                for _ in 1 .. 9: # There are 9 nodes in a Shift
                    # parse Shift
                    var shift: Shift
                    shift.quoted = file.readLine() == "1"
                    shift.date = times.parse(file.readLine(), DATE_FORMAT)
                    shift.description = file.readLine()
                    let startStr = file.readLine()
                    shift.start = if startStr == "?": none(DateTime) else: some(
                            times.parse(startStr, DATE_TIME_FORMAT))
                    let finishStr = file.readLine()
                    shift.finish = if finishStr == "?": none(
                            DateTime) else: some(times.parse(finishStr,
                                    DATE_TIME_FORMAT))
                    let breakStr = file.readLine()
                    shift.breakTime = if breakStr == "?": none(
                            Duration) else: some(initDuration(
                                    minutes = breakStr.parseBiggestInt()))
                    shift.rate = file.readLine().parseFloat()
                    let qtyStr = file.readLine()
                    shift.qty = if qtyStr == "?": none(float) else: some(
                            qtyStr.parseFloat())
                    let id = file.readLine().parseBiggestInt()
                    shift.id = id
                    data.shifts[id] = shift
                    echo "Shift deserialized: ", $shift
            except EOFError:
                echo "EOF reached"
                return
    except EOFError:
        echo "Empty serialization file"
    except:
        echo "Serialization file has a loss of integrity, backing up and removing file"
        copyFile(AppDataFilePath, AppDataFilePath & "." & $getTime().toUnix() & ".BAK")
        removeFile(AppDataFilePath)



proc dumpData*(data: Data) =
    echo "Dumping data..."
    var file = open(AppDataFilePath, fmWrite)
    if data.shifts.len() == 0: 
        removeFile AppDataFilePath
    else:
        try:
            file.writeLine(data.balance)
            for shiftID, shift in data.shifts:
                # Add 9 fields seperated by newlines
                file.writeLine(if shift.quoted: 1 else: 0)
                file.writeLine(shift.date.format(DATE_FORMAT))
                file.writeLine(shift.description)
                file.writeLine(if shift.start.isSome(): shift.start.get().format(
                        DATE_TIME_FORMAT) else: "?")
                file.writeLine(if shift.finish.isSome(): shift.finish.get().format(
                        DATE_TIME_FORMAT) else: "?")
                file.writeLine(if shift.breakTime.isSome(): $shift.breakTime.get().inMinutes() else: "?")
                file.writeLine(shift.rate)
                file.writeLine(if shift.qty.isSome(): $shift.qty.get() else: "?")
                file.writeLine(shift.id)
            echo "Data dumped"
            echo readFile AppDataFilePath
        except:
            echo "Deserialization error: ", getCurrentException().msg