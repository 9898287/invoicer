import times, strutils, options, strformat, hashes

# Tries to filter all the non numerical
# prefix and postfix characters and try
# return a float
proc tolerantFloatParse*(s: string): float64 =
    var rectifiedString: string
    for c in s:
        if c.isDigit() or c == '.':
            rectifiedString &= c
    result = rectifiedString.parseFloat()

type
    Shift* = object
        quoted*: bool
        date*: DateTime
        description*: string
        start*: Option[DateTime]
        finish*: Option[DateTime]
        breakTime*: Option[Duration]
        rate*: float
        qty*: Option[float]
        id*: int64

proc hash*(shift: Shift): Hash =
  hash(shift.id)
 
proc `==`*(lhs, rhs: Shift): bool =
  lhs.id == rhs.id
  
proc intToMonth(monthNumber: 0..11): Month =
    case monthNumber:
        of 0: return mJan
        of 1: return mFeb
        of 2: return mMar
        of 3: return mApr
        of 4: return mMay
        of 5: return mJun
        of 6: return mJul
        of 7: return mAug
        of 8: return mSep
        of 9: return mOct
        of 10: return mNov
        of 11: return mDec

# Failible: MUST CATCH EXCEPTION
proc newShift*(quoted: bool,
                dateDay: 1..31,
                dateMonth: 0..11,
                dateYear: int,
                description: string,
                rate: float,
                startHour: Option[range[1 .. 12]],
                startMinute: Option[range[0 .. 59]],
                startAMPM: Option[string],
                finishHour: Option[range[1 .. 12]],
                finishMinute: Option[range[0 .. 59]],
                finishAMPM: Option[string],
                breakTime: Option[float],
                qty: Option[float],
                id = getTime().toUnix()): Shift =
    result.quoted = quoted
    result.date = initDateTime(dateDay, intToMonth(dateMonth), dateYear, 0, 0,
            0, 0)
    result.description = description
    result.rate = rate
    if not quoted:
        # parsing is less boilerplate
        let
            start = some(parse(fmt"{startHour.get()}:{startMinute.get()} {startAMPM.get()}", "h:m tt"))
            finish = some(parse(fmt"{finishHour.get()}:{finishMinute.get()} {finishAMPM.get()}", "h:m tt"))
        result.start = start
        result.finish = finish
        if start.isSome() and finish.isSome() and result.start.get() >
                result.finish.get():
            raise newException(CatchableError, "Finish time is earlier than Start time")
        result.breakTime = some(initDuration(minutes = int(breakTime.get() * 60.0)))
        if result.breakTime.get() > finish.get() - start.get():
            raise newException(CatchableError, "Break time is longer than billable hours")
        # qty will default to None[float]
    else:
        # everything else will default to None[T]
        result.qty = qty
    result.id = id

proc billableHours*(shift: Shift): Option[Duration] =
    if shift.quoted: none(Duration)
    else: some(shift.finish.get() - shift.start.get() - shift.breakTime.get())

proc payable*(shift: Shift): float64 =
    if not shift.quoted:
        (shift.rate / 60.0) * float64(
            shift.billableHours().get().inMinutes())
    else:
        shift.rate * shift.qty.get()

proc getDateString*(shift: Shift): string =
    shift.date.format("dd MMM yy")

proc getStartTimeString*(shift: Shift): string =
    if not shift.quoted: shift.start.get().format("hh:mm tt") else: "-"

proc getFinishTimeString*(shift: Shift): string =
    if not shift.quoted: shift.finish.get().format("hh:mm tt") else: "-"

proc getBreakTimeString*(shift: Shift): string =
    if not shift.quoted:
        let t = shift.breakTime.get().inMinutes().float() / 60.0
        result = t.formatFloat(ffDecimal, precision = 2)
    else:
        result = "-"

proc getBillableHoursOrQtyString*(shift: Shift): string =
    if not shift.quoted:
        let t = shift.billableHours().get().inMinutes().float() / 60.0
        result = t.formatFloat(ffDecimal, precision = 2)
    else:
        result = shift.qty.get().formatFloat(ffDecimal, precision = 2)

proc getRateString*(shift: Shift): string =
    "$ " & shift.rate.formatFloat(ffDecimal, precision = 2)

proc getPayableString*(shift: Shift): string =
    "$ " & shift.payable().formatFloat(ffDecimal, precision = 2)
