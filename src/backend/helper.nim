import os
import gintro/glib

let
    RuntimeDirSep* = if defined windows: """\""" else: "/"
    AppDataFolder = if defined posix:
        getUserDataDir()
    else:
        getEnv("APPDATA")
    AppDataFilePath* = AppDataFolder & RuntimeDirSep & "com.gitlab.adnan338.Invoicer"
    XdgOpen* = if defined linux:
        "xdg-open"
    elif defined macosx:
        "open"
    elif defined windows:
        "start"
    else: ""

proc getAppVersion(): string =
    "1.1.0"

let AppVersion* = getAppVersion()
