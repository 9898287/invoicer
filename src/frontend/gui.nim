import times, tables, locks, times, strutils, options, os
import gintro/[gobject, gtk, glib]
import ../backend/[jobunit, datastate, exporter, helper]

const GladeStr = readFile("resources" & DirSep & "ui.glade")

type
    TableColumns* = enum
        Date = "Date"
        Details = "Address/Description"
        Start = "Start"
        End = "End"
        Break = "Break"
        Rate = "Houly/Unit Rate"
        Hours = "Billable Hours/Unit Qty"
        Payable = "Payable"
        ID = "ID"

    UI* = object
        builder*: Builder
        window*: ApplicationWindow
        addBtn*: Button
        popoverWidget*: Popover
        ls*: gtk.ListStore
        tv*: TreeView
        sw*: ScrolledWindow
        calendar*: Calendar
        description*: Entry
        startHour*: SpinButton
        startMinute*: SpinButton
        startAMPM*: ComboBoxText
        endHour*: SpinButton
        endMin*: SpinButton
        endAMPM*: ComboBoxText
        breakTime*: SpinButton
        hourlyRate*: SpinButton
        timedDoneButton*: Button
        unitQty*: SpinButton
        unitRate*: SpinButton
        quotedDoneButton*: Button
        removeButton*: Button
        balanceButton*: Button
        balanceSpin*: SpinButton
        balanceLabel*: Label
        balancePopover*: Popover
        updateButton*: Button
        errorDialog*: MessageDialog
        nextBtn*: Button
        nextDialog*: Dialog
        invoiceTo*: Entry
        invoiceFrom*: Entry
        fcbLabel*: Label
        fcb*: Button
        fcn*: FileChooserNative
        confirmBtn*: Button
        successDiag*: Dialog

#### GLOBALLY MUTABLE VARIABLE ####
var dataLock*: Lock
initLock(dataLock)
var data*: AppData
#### GLOBALLY MUTABLE VARIABLE ####

#### START CALLBACKS ####
proc popupEvent(_: Button; ui: UI) =
    ui.popoverWidget.popup()

proc toStringVal(s: string): Value =
    let gtype = typeFromName("gchararray")
    discard init(result, gtype)
    setString(result, s)

proc toInt64Val(i: int64): Value =
    let gtype = typeFromName("gint64")
    discard init(result, gtype)
    setInt64(result, i)

proc calculateTotalePayable(ui: UI): float =
    var
        totalPayable = 0.0
        balance: float
    try:
        balance = ui.balanceLabel.getLabel().tolerantFloatParse()
    except:
        balance = 0
    withLock dataLock:
        for shift in data.shifts.values():
            totalPayable += shift.payable()
    result = totalPayable - balance

proc calculateTotalHours(ui: UI): Option[float] =
    var
        totalHours = 0.0
    try:
        withLock dataLock:
            for shift in data.shifts.values():
                totalHours += (shift.billableHours().get().inMinutes().float() / 60.0)
        result = some(totalHours)
    except:
        result = none(float)

proc setTotalPayable(ui: UI) =
    ui.builder.getLabel("summary_payable").setLabel("Net Payable: $ " &
            $ui.calculateTotalePayable().formatFloat(ffDecimal, 2))

proc setTotalHours(ui: UI) =
    let hrs = ui.calculateTotalHours()
    if hrs.isSome():
        ui.builder.getLabel("summary_hours").setLabel("(" &
                $ui.calculateTotalHours().get().formatFloat(ffDecimal, 2) & " Hrs)")
    else:
        ui.builder.getLabel("summary_hours").setLabel("")

proc setTotalValues(ui: UI) =
    ui.setTotalPayable()
    try:
        ui.setTotalHours()
    except:
        echo getCurrentException().msg
        discard # unimportant error

proc popoverClosedEvent(_: Popover; ui: UI) =
    ui.setTotalValues()

proc loadShift*(model: ListStore; shift: Shift) =
    var iter: TreeIter
    model.append(iter)
    model.setValue(iter, TableColumns.Date.ord(), shift.getDateString().toStringVal())
    model.setValue(iter, TableColumns.Details.ord(),
            shift.description.toStringVal())
    model.setValue(iter, TableColumns.Start.ord(),
            shift.getStartTimeString().toStringVal())
    model.setValue(iter, TableColumns.End.ord(),
            shift.getFinishTimeString().toStringVal())
    model.setValue(iter, TableColumns.Break.ord(),
            shift.getBreakTimeString().toStringVal())
    model.setValue(iter, TableColumns.Hours.ord(),
            shift.getBillableHoursOrQtyString().toStringVal())
    model.setValue(iter, TableColumns.Rate.ord(),
            shift.getRateString().toStringVal())
    model.setValue(iter, TableColumns.Payable.ord(),
            shift.getPayableString().toStringVal())
    model.setValue(iter, TableColumns.ID.ord(), shift.id.toInt64Val())

proc timedDoneButtonEvent(_: Button; ui: UI) =
    defer: ui.popoverWidget.popdown()
    try:
        var
            dateYear, dateMonth, dateDay: int
        ui.calendar.getDate(dateYear, dateMonth, dateDay)
        let
            description = ui.description.getText()
            rate = ui.hourlyRate.getValue()
            startHour = some[range[1 .. 12]](ui.startHour.getValueAsInt())
            startMinute = some[range[0 .. 59]](ui.startMinute.getValueAsInt())
            startAMPM = some(ui.startAMPM.getActiveText())
            endHour = some[range[1 .. 12]](ui.endHour.getValueAsInt())
            endMin = some[range[0 .. 59]](ui.endMin.getValueAsInt())
            endAMPM = some(ui.endAMPM.getActiveText())
            breakTime = some(ui.breakTime.getValue())
            qty = none(float)
            shift = newShift(false,
                dateDay,
                dateMonth,
                dateYear,
                description,
                rate,
                startHour,
                startMinute,
                startAMPM,
                endHour,
                endMin,
                endAMPM,
                breakTime,
                qty)
        withLock dataLock: 
            data.shifts[shift.id] = shift
            data.dumpData()
        echo "Shift created: ", $shift
        let model = ui.ls
        model.loadShift(shift)
        ui.popoverWidget.popdown()
    except:
        echo "Invalid input: " & getCurrentExceptionMsg()
        let errorDialog = ui.errorDialog
        discard errorDialog.run()
        # Currently gintro does not provide wrappers for Gtk.MessageDialog
        # As a result I am unable to dynamically set the error message
        # with gtk_message_dialog_format_secondary_text()
        errorDialog.hide()

proc quotedDoneButtonEvent(_: Button; ui: UI) =
    defer: ui.popoverWidget.popdown()
    try:
        var
            dateYear, dateMonth, dateDay: int
        ui.calendar.getDate(dateYear, dateMonth, dateDay)
        let
            description = ui.description.getText()
            rate = ui.unitRate.getValue()
            startHour = none[range[1 .. 12]]()
            startMinute = none[range[0 .. 59]]()
            startAMPM = none(string)
            endHour = none[range[1 .. 12]]()
            endMin = none[range[0 .. 59]]()
            endAMPM = none(string)
            breakTime = none(float)
            qty = some(ui.unitQty.getValue())
            shift = newShift(true,
                dateDay,
                dateMonth,
                dateYear,
                description,
                rate,
                startHour,
                startMinute,
                startAMPM,
                endHour,
                endMin,
                endAMPM,
                breakTime,
                qty)
        withLock dataLock: 
            data.shifts[shift.id] = shift
            data.dumpData()
        echo "Shift created: ", $shift
        let model = ui.ls
        model.loadShift(shift)
        ui.popoverWidget.popdown()
    except:
        echo "Invalid input: " & getCurrentExceptionMsg()
        let errorDialog = ui.errorDialog
        discard errorDialog.run()
        errorDialog.hide()

proc removeButtonEvent(_: Button; ui: UI) =
    var
        selection = ui.tv.getSelection()
        ls: ListStore
        iter: TreeIter
    let model = ui.ls
    if not model.getIterFirst(iter):
        return
    if getSelected(selection, ls, iter):
        var shiftIDValue: Value
        model.getValue(iter, TableColumns.ID.ord(), shiftIDValue)
        let shiftID = shiftIDValue.getInt64()
        echo "Removing shift ID: ", shiftID
        ui.ls.clear()
        withLock dataLock:
            data.shifts.del(shiftID)
            data.dumpData()
            for _, shift in data.shifts:
                ui.ls.loadShift(shift)
        # discard model.remove(iter)
    ui.setTotalValues()

proc balanceButtonEvent(_: Button; ui: UI) =
    ui.balancePopover.popup()

proc balanceUpdateEvent(_: Button; ui: UI) =
    let balance = ui.balanceSpin.getValue()
    withLock dataLock: 
        data.balance = ui.balanceSpin.getValue()
        data.dumpData()
    ui.balanceLabel.setLabel("$ " & $balance.formatFloat(
            ffDecimal, 2))
    ui.setTotalPayable()
    ui.balancePopover.popDown()

proc balancePopoverClosedEvent(_: Popover; ui: UI) =
    ui.setTotalPayable()
    discard

proc nextButtonEvent(_: Button; ui: UI) =
    let diag = ui.nextDialog
    discard diag.run()
    diag.hide()

proc confirmButtonEvent(_: Button; ui: UI) =
    withLock dataLock:
        try:
            let
                invoiceTo = ui.invoiceTo.getText()
                invoicedFrom = ui.invoiceFrom.getText()
                exportTo = ui.fcn.getFilename()
            let fname = exportHTML(data, invoiceTo, invoicedFrom, exportTo)
            ui.nextDialog.hide()
            if XdgOpen != "":
                try:
                    discard glib.spawnCommandLineAsync(XdgOpen & " \'" & fname & "\'")
                except:
                    discard
            discard ui.successDiag.run()
            ui.successDiag.hide()
        except:
            echo "Export error: " & getCurrentException().msg
            discard ui.errorDialog.run()
            ui.errorDialog.hide()

proc fcbEvent(_: Button; ui: UI) =
    let resp = ui.fcn.run()
    if resp == ResponseType.accept.ord():
        ui.fcbLabel.setLabel(ui.fcn.getFileName())

#### END CALLBACKS ####

proc initCallbacks*(ui: UI) =
    ui.addBtn.connect("clicked", popupEvent, ui)
    ui.removeButton.connect("clicked", removeButtonEvent, ui)
    ui.popoverWidget.connect("closed", popoverClosedEvent, ui)
    ui.timedDoneButton.connect("clicked", timedDoneButtonEvent, ui)
    ui.quotedDoneButton.connect("clicked", quotedDoneButtonEvent, ui)
    ui.balanceButton.connect("clicked", balanceButtonEvent, ui)
    ui.updateButton.connect("clicked", balanceUpdateEvent, ui)
    ui.balancePopover.connect("closed", balancePopoverClosedEvent, ui)
    ui.nextBtn.connect("clicked", nextButtonEvent, ui)
    ui.fcb.connect("clicked", fcbEvent, ui)
    ui.confirmBtn.connect("clicked", confirmButtonEvent, ui)

proc showAll(ui: UI) =
    ui.window.showAll()

proc generateTreeView(ui: UI): TreeView =
    result = newTreeView()
    result.setHeadersVisible(true)
    var idx = 0
    for column in TableColumns:
        let
            renderer = newCellRendererText()
            tvColumn = newTreeViewColumn()
        tvColumn.setTitle($column)
        tvColumn.packStart(renderer, true)
        tvColumn.addAttribute(renderer, "text", idx)
        tvColumn.setVisible(idx != TableColumns.ID.ord())
        discard result.appendColumn(tvColumn)
        inc idx

proc newUI(app: Application): UI =
    let builder = newBuilderFromString(GladeStr)
    result.builder = builder
    let window = builder.getApplicationWindow("app_window")
    result.window = window
    result.window.setApplication(app)
    result.builder.getHeaderBar("hb").setSubtitle("Version: " & AppVersion)
    let cal = builder.getCalendar("calendar")
    let today = now()
    # times.MonthRange starts with 1
    cal.selectMonth(today.month().ord() - 1, today.year())
    # times.MonthDayRange starts with 1
    cal.selectDay(today.monthday() - 1)
    let popoverWidget = builder.getPopover("popover")
    result.popoverWidget = popoverWidget
    let addButton = builder.getButton("add")
    result.addBtn = addButton
    result.calendar = cal
    # TODO automate this
    let listType = [typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gchararray"),
        typeFromName("gint64")] # ID, this will not be shown in the treeview
    let ls = newListStore(listType.len(), cast[ptr GType](
            unsafeAddr listType))
    withLock dataLock:
        loadData(data)
        echo "Data length: ", data.shifts.len()
        for _, shift in data.shifts: ls.loadShift(shift)
    result.ls = ls
    result.tv = generateTreeView(result)
    result.tv.setModel(result.ls)
    result.sw = builder.getScrolledWindow("scrolled_window")
    result.sw.add(result.tv)
    result.description = builder.getEntry("description")
    result.startHour = builder.getSpinButton("start_hr")
    result.startMinute = builder.getSpinButton("start_min")
    result.startAMPM = builder.getComboBoxText("start_ampm")
    result.endHour = builder.getSpinButton("end_hr")
    result.endMin = builder.getSpinButton("end_min")
    result.endAMPM = builder.getComboBoxText("end_ampm")
    result.hourlyRate = builder.getSpinButton("hourly_rate")
    result.breakTime = builder.getSpinButton("break_time")
    result.timedDoneButton = builder.getButton("time_billed_done_btn")
    result.unitQty = builder.getSpinButton("unit_qty")
    result.unitRate = builder.getSpinButton("unit_rate")
    result.quotedDoneButton = builder.getButton("quoted_done_btn")
    result.removeButton = builder.getButton("remove")
    result.balanceButton = builder.getButton("balance_btn")
    result.balanceSpin = builder.getSpinButton("balance_spin")
    result.balanceLabel = builder.getLabel("balance_label")
    result.balanceLabel.setLabel("$ 0.00")
    result.balancePopover = builder.getPopover("balance_popover")
    result.updateButton = builder.getButton("balance_update")
    result.errorDialog = builder.getMessageDialog("error_msg_dialog")
    result.nextDialog = builder.getDialog("name_dialog")
    result.invoiceTo = builder.getEntry("invoice_to")
    result.invoiceFrom = builder.getEntry("invoice_from")
    result.fcbLabel = builder.getLabel("fcb_label")
    result.fcb = builder.getButton("fcb")
    result.nextBtn = builder.getButton("next")
    result.fcn = newFileChooserNative("Save your invoice", window,
            FileChooserAction.selectFolder, "Select folder", "Cancel")
    result.fcn.setCreateFolders(true)
    result.confirmBtn = builder.getButton("confirm")
    result.successDiag = builder.getDialog("success_msg_dialog")

proc appActivate*(app: Application) =
    let ui = newUI(app)
    ui.initCallbacks()
    ui.showAll()
