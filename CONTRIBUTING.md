Thank you for choosing to contributing to Invoicer. 
Please ensure the following before sending any patches:

*  Please bump up the version of the app in: 
    - `invoicer.nimble`, 

    - `src/backend/helper/getAppVersion()`, 

    - and `com.gitlab.adnan338.Invoicer.metainfo.xml`. Make sure these version numbers match.

* Please do not push codes that are not cross platform, 
avoid any windows/linux/macos specific API. If you do 
please ensure you take steps to call alternative APIs 
available in other platforms. Try to contain these calls 
within `src/backend/helper.nim`.

* Format your code and do not push build-artifacts.

