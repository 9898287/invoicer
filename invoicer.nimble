# Package

version       = "1.1.0"
author        = "adnan338"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["invoicer"]
binDir        = "bin"

# Dependencies

requires    "nim >= 1.4.2",
            "gintro 0.8.5"

